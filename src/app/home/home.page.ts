import { Component, NgZone } from "@angular/core";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { Platform } from "@ionic/angular";
import { LoadingController } from "@ionic/angular";
import { AlertController } from "@ionic/angular";
import { interval, Subscription } from "rxjs";
import { StorageService } from "../storage.service";
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';

declare var google: any;
@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage {
  userLocations = [];
  fetchLocationSubscription: Subscription;
  loading;
  constructor(
    private zone: NgZone,
    private geolocation: Geolocation,
    private platform: Platform,
    private loadingController: LoadingController,
    private alertController: AlertController,
    private storageService: StorageService,
    private locationAccuracy: LocationAccuracy
  ) {
    this.storageService.getObject("user_location")
      .then(result => result != null ? this.userLocations = JSON.parse(result) : null)
      .catch(e => this.showLoader("Error Fetching Data"));
  }

  onFetchUserLocation() {
    this.platform.ready().then(() =>{
      if (this.platform.is('cordova')) {
         // When GPS Turned ON call method getUserLocation()
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY)
          .then( () => this.getUserLocation(),
          error => this.showLoader("Error requesting location permissions")
        );
      }else{
        this.getUserLocation()
      }
    });
  }

  async getUserLocation() {
    this.loading = await this.loadingController.create({
      message: "Retrieving your current location...",
      duration: 1000,
      spinner: "crescent"
    });
    await this.loading.present();
    this.geolocation.getCurrentPosition()
      .then(resp => this.getGeoLocation(resp.coords.latitude, resp.coords.longitude))
      .catch(error => {
        this.loading.dismiss();
        this.showLoader("Error getting location");
    });
  }

  async getGeoLocation(lat: number, lng: number) {
    if (navigator.geolocation) {
      let geocoder = await new google.maps.Geocoder();
      let latlng = await new google.maps.LatLng(lat, lng);
      let request = { latLng: latlng };
      await geocoder.geocode(request, (results, status) => {
        if (status == google.maps.GeocoderStatus.OK) {
          let result = results[0];
          this.zone.run(() => {
            if (result != null) {
              this.loading.dismiss();
              this.userLocations.unshift({
                time: new Date(),
                latitude: lat,
                longitude: lng,
                city: result.address_components[3].short_name,
                locality: result.address_components[2].short_name,
                subAddress: result.address_components[1].short_name,
                address: result.address_components[0].short_name
              });
              this.storageService.setObject( "user_location", JSON.stringify(this.userLocations));
            }
          });
        }
      });
    }
  }

  async showLoader(msg) {
    const alert = await this.alertController.create({
      message: msg,
      buttons: ["OK"]
    });
    await alert.present();
  }

  async onClearAllLocation(){
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      duration: 500
    });
    await loading.present();
    setTimeout(() => {
      loading.onDidDismiss();
      this.userLocations = [];
      this.storageService.clear();
    }, 500);
  }

  ngOnInit() {
    // Fetch location after every 15minutes
    this.fetchLocationSubscription = interval(600000).subscribe(x => this.onFetchUserLocation());
    // On First Load Fetch Location
    setTimeout(() => { this.onFetchUserLocation(); }, 500);
  }

  ngOnDestroy() {
    this.fetchLocationSubscription.unsubscribe();
  }

}
