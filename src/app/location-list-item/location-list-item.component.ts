import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "location-list-item",
  templateUrl: "./location-list-item.component.html",
  styleUrls: ["./location-list-item.component.scss"]
})
export class LocationListItemComponent implements OnInit {
  @Input() userLocation;

  constructor() {}

  ngOnInit() {}
}
